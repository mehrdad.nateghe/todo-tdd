<?php

namespace Nateghi\Todo\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Support\Str;

class LabelTask extends Pivot
{
	protected $fillable = ['user_id'];

	public static function boot() {
		parent::boot();
		static::creating(function($tag) {
			$tag->user_id = auth()->id();
		});
	}
}
