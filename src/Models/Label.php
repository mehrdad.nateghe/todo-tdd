<?php

namespace Nateghi\Todo\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Label extends Model
{
	protected $fillable = ['title','uuid'];

	protected $table = 'labels';

	public function getRouteKeyName()
	{
		return 'uuid';
	}

	public function tasks()
	{
		return $this->morphedByMany(Task::class, 'labelable');
	}

	protected static function boot()
	{
		parent::boot();

		static::creating(function ($model) {
			$model->uuid = (string) Str::uuid();
		});
	}
}
