<?php

namespace Nateghi\Todo\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;

class Task extends Model
{

	protected $fillable = ['title','description','status','uuid','user_id'];

	public function getRouteKeyName()
	{
		return 'uuid';
	}

	public function labels()
	{
		return $this->morphToMany(Label::class, 'labelable');
	}

	protected static function boot()
	{
		parent::boot();

		static::creating(function ($model) {
			$model->uuid = (string) Str::uuid();
		});
	}
}
