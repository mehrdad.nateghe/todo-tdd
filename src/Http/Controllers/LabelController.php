<?php

namespace Nateghi\Todo\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Nateghi\Todo\Models\Label;

class LabelController extends Controller
{
	public function index()
	{
		$labels = Label::all();

		$data = [];

		foreach ($labels as $label){
			array_push($data, [
				'id' => $label->id,
				'label' => $label->title,
				'total_tasks' => $label->tasks()->where('user_id', Auth::id())->count(),
			]);
		}

		return response()->json([
			'message' => Response::$statusTexts[Response::HTTP_ACCEPTED],
			'data'    => [
				'labels' => $data
			],
		],Response::HTTP_ACCEPTED);
	}

	public function show()
	{
		//
	}

	public function store(Request $request)
	{
		$request->validate([
			'title' => 'required|unique:labels,title'
		]);

		$label = Label::create([
			'title' => $request->get('title'),
		]);

		return response()->json([
			'message' => Response::$statusTexts[Response::HTTP_CREATED],
			'data'    => [
				'label' => $label
			],
		],Response::HTTP_CREATED);
	}
}
