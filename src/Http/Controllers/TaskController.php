<?php

namespace Nateghi\Todo\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Nateghi\Todo\Label;
use Nateghi\Todo\Notifications\TaskCloseEmailNotification;
use Nateghi\Todo\Notifications\TaskCloseLogNotification;
use Nateghi\Todo\Notifications\TaskCloseNotification;
use Nateghi\Todo\Models\Task;

class TaskController extends Controller
{
	public function index()
	{
		$user  = Auth::user();
		$tasks = $user->tasks()->get();

		$data = [];

		foreach ($tasks as $task) {

			$labels = [];
			foreach ($task->labels()->get() as $label) {
				array_push($labels,[
					'id'          => $label->id,
					'label'       => $label->title,
					'total_tasks' => $label->tasks()->where('user_id',Auth::id())->count(),
				]);
			}

			array_push($data,[
				'id'          => $task->id,
				'title'       => $task->title,
				'description' => $task->description,
				'labels'      => $labels,
			]);
		}

		return response()->json([
			'message' => Response::$statusTexts[Response::HTTP_ACCEPTED],
			'data'    => [
				'labels' => $data
			],
		],Response::HTTP_ACCEPTED);
	}

	public function show()
	{
		$user  = Auth::user();
		$tasks = $user->tasks()->select('title','description','status')->get();

		return response()->json([
			'message' => Response::$statusTexts[Response::HTTP_OK],
			'data'    => [
				'tasks' => $tasks
			],
		],Response::HTTP_OK);

	}

	public function store(Request $request)
	{
		$request->validate([
			'title'       => 'required',
			'description' => 'required',
			'status'      => 'required|in:close,open',
		]);

		$user = Auth::user();
		$task = $user->tasks()->create([
			'title'       => $request->get('title'),
			'description' => $request->get('description'),
			'status'      => $request->get('status') === 'open' ? 1 : 0,
		]);

		return response()->json([
			'message' => Response::$statusTexts[Response::HTTP_CREATED],
			'data'    => [
				'task' => $task
			],
		],Response::HTTP_CREATED);
	}

	public function update(Task $task,Request $request)
	{
		$request->validate([
			'title'       => 'required',
			'description' => 'required',
		]);

		$user = Auth::user();

		if($user->id !== $task->user_id){
			return response()->json([
				'error' => Response::$statusTexts[Response::HTTP_UNAUTHORIZED],
			],Response::HTTP_UNAUTHORIZED);
		}

		$task->update([
			'title'       => $request->get('title'),
			'description' => $request->get('description'),
		]);

		return response()->json([
			'message' => Response::$statusTexts[Response::HTTP_ACCEPTED],
			'data'    => [
				'task' => $task
			],
		],Response::HTTP_ACCEPTED);
	}

	public function changeStatus(Task $task,Request $request)
	{
		$request->validate([
			'status' => 'required|in:close,open',
		]);

		$user = Auth::user();

		if($user->id !== $task->user_id){
			return response()->json([
				'error' => Response::$statusTexts[Response::HTTP_UNAUTHORIZED],
			],Response::HTTP_UNAUTHORIZED);
		}

		$task->update([
			'status' => $request->get('status') === 'open' ? 1 : 0
		]);

		if($request->get('status') == 'close'){
			Notification::send($user, new TaskCloseEmailNotification($task));
		}

		return response()->json([
			'message' => Response::$statusTexts[Response::HTTP_ACCEPTED],
			'data'    => [
				'task' => $task
			],
		],Response::HTTP_ACCEPTED);
	}

	public function addLabels(Task $task,Request $request)
	{
		$request->validate([
			'labels' => 'required',
		]);

		$labels = $request->get('labels');

		$labelsId = [];

		foreach ($labels as $label) {
			$existLabel = Label::query()->where('title',$label)->first();

			if($existLabel){
				array_push($labelsId,$existLabel->id);
			}else{
				$newLabel = Label::create([
					'title' => $label
				]);

				array_push($labelsId,$newLabel->id);
			}
		}

		$tasks = $task->labels()->sync($labelsId);

		return response()->json([
			'message' => Response::$statusTexts[Response::HTTP_ACCEPTED],
		],Response::HTTP_ACCEPTED);
	}
}
