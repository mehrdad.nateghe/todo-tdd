<?php

namespace Nateghi\Todo;

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Nateghi\Todo\Http\Middleware\ApiToken;

class TodoServiceProvider extends ServiceProvider
{
	public function register()
	{
		$this->mergeConfigFrom(__DIR__ . '/Config/todo.php','todo');
	}

	public function boot()
	{
		$this->publishes([
			__DIR__ . '/Config/todo.php' => config_path('todo.php'),
		],'config');

		$this->loadMigrationsFrom(__DIR__ . '/Migrations');

		/*$this->publishes([
			realpath(__DIR__.'/../Migrations') => database_path('migrations/')
		], 'migrations');

		$this->publishes([
			__DIR__ . '/../Migrations/' => database_path('migrations/my-package'),
		], 'my-package-migrations');*/

		$this->publishes([
			__DIR__.'/Migrations/' => database_path('migrations')
		], 'migrations');

		$router = $this->app->make(Router::class);
		$router->aliasMiddleware('apiToken',ApiToken::class);
		$this->registerRoutes();
	}

	protected function registerRoutes()
	{
		Route::group($this->routeConfiguration(),function () {
			$this->loadRoutesFrom(__DIR__ . '/Routes/api.php');
		});
	}

	protected function routeConfiguration()
	{
		return [
			'prefix'     => config('todo.route_prefix'),
			'middleware' => config('todo.route_middleware'),
		];
	}
}
