<?php

namespace Nateghi\Todo\Traits;

use Nateghi\Todo\Models\Task;

trait Taskable
{
	public function initializeTaskable()
	{
		$this->fillable[] = 'api_token';
	}

	public function getToken()
	{
		return $this->api_token;
	}

	public function tasks()
	{
		return $this->hasMany(Task::class);
	}
}