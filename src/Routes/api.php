<?php

use Illuminate\Support\Facades\Route;
use Nateghi\Todo\Http\Controllers\LabelController;
use Nateghi\Todo\Http\Controllers\TaskController;

// labels
Route::resource('labels',LabelController::class);


//Route::post('/labels', [LabelController::class,'store'])->name('labels.store');

// tasks
Route::resource('tasks',TaskController::class);
Route::post('/tasks/{task}/change/status', [TaskController::class,'changeStatus'])->name('tasks.change.status');
Route::post('/tasks/{task}/add/labels', [TaskController::class,'addLabels'])->name('tasks.add.labels');
