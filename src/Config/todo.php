<?php

return [
	'user_table'       => 'users',
	'user_model'       => App\User::class,
	'route_prefix'     => '/api/todo',
	'route_middleware' => ['auth:api'],
];
