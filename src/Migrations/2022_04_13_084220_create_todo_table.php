<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTodoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	$userTable = config('todo.user_table');

		Schema::table($userTable, function (Blueprint $table) {
			$table->string('api_token')->nullable()->unique()->after('password');
		});

        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
			$table->foreignId('user_id')->constrained();
            $table->text('title')->nullable();
            $table->longText('description')->nullable();
            $table->boolean('status')->nullable();
            $table->timestamps();
        });

		Schema::create('labels', function (Blueprint $table) {
			$table->id();
			$table->uuid('uuid')->unique();
			$table->string('title')->unique();
			$table->timestamps();
		});

		Schema::create('labelables', function (Blueprint $table) {
			$table->bigInteger("label_id");
			$table->bigInteger("labelable_id");
			$table->string("labelable_type");
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		$userTable = config('todo.user_table');

		Schema::table($userTable, function (Blueprint $table) {
			$table->dropColumn('api_token');
		});
        Schema::dropIfExists('tasks');
        Schema::dropIfExists('labels');
        Schema::dropIfExists('labelables');
    }
}
