<?php

namespace Nateghi\Todo\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Nateghi\Todo\Models\Task;

class TaskCloseEmailNotification extends Notification
{
	use Queueable;

	public $task;

	public function __construct(Task $task)
	{
		$this->task = $task;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param mixed $notifiable
	 * @return array
	 */
	public function via($notifiable)
	{
		return ['mail'];
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param mixed $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable)
	{
		$taskTitle = $this->task->title;

		return (new MailMessage)
			->line("Task: {$taskTitle}")
			->line("Status: Close")
			->action('Show task',url('/'))
			->line('Thank you for using our application!');
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param mixed $notifiable
	 * @return array
	 */
	public function toArray($notifiable)
	{
		return [
			//
		];
	}
}
