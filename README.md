
## Installation

1- Install todo package
```bash
  composer require nateghi/todo
```

2- Publish vendor
```bash
  php artisan vendor:publish --provider="Nateghi\Todo\TodoServiceProvider"
```

3- Run migrations
```bash
  php artisan migrate
```

4- Set your config
```bash
  config/todo.php
```

5- Add Trait to User model
```bash
  use Taskable;
```

5- Routes
```bash
  php artisan route:list
```

6- Run tests
```bash
  php artisan test vendor/nateghi/todo/tests
```
    