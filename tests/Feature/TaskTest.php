<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;
use Nateghi\Todo\Models\Task;
use Nateghi\Todo\Notifications\TaskCloseEmailNotification;
use Tests\TestCase;

class TaskTest extends TestCase
{
	use RefreshDatabase;
	use WithFaker;

	public function test_guest_users_cannot_create_a_new_task()
	{
		$response = $this->withHeaders([
			'Authorization' => 'Bearer ' . 'wrong',
		])->json('POST','api/todo/tasks',[
			'title' => $this->faker->text('20'),
		]);

		$response->assertStatus(Response::HTTP_UNAUTHORIZED);
	}

	public function test_create_task()
	{
		$token = $this->authenticate();

		$response = $this->withHeaders([
			'Authorization' => 'Bearer ' . $token,
		])->json('POST','api/todo/tasks',[
			'title'       => $this->faker->text('20'),
			'description' => $this->faker->text('200'),
			'status'      => $this->faker->randomElement(['open','close']),
		]);

		$response->assertStatus(Response::HTTP_CREATED);
	}

	protected function authenticate()
	{
		$user = $this->getUserModel()::create([
			'name'      => $this->faker->name(),
			'email'     => $this->faker->email(),
			'password'  => $this->faker->password(),
			'api_token' => Hash::make(Str::random(32)),
		]);

		return $user->api_token;
	}

	public function getUserModel()
	{
		return config('todo.user_model');
	}

	public function test_cannot_create_task_with_empty_title()
	{
		$token = $this->authenticate();

		$response = $this->withHeaders([
			'Authorization' => 'Bearer ' . $token,
		])->json('POST','api/todo/tasks',[
			'title' => '',
		]);

		$response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
	}

	public function test_cannot_create_task_with_empty_description()
	{
		$token = $this->authenticate();

		$response = $this->withHeaders([
			'Authorization' => 'Bearer ' . $token,
		])->json('POST','api/todo/tasks',[
			'description' => '',
		]);

		$response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
	}

	public function test_cannot_create_task_with_empty_status()
	{
		$token = $this->authenticate();

		$response = $this->withHeaders([
			'Authorization' => 'Bearer ' . $token,
		])->json('POST','api/todo/tasks',[
			'status' => '',
		]);

		$response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
	}

	public function test_cannot_create_task_with_wrong_status()
	{
		$token = $this->authenticate();

		$response = $this->withHeaders([
			'Authorization' => 'Bearer ' . $token,
		])->json('POST','api/todo/tasks',[
			'status' => 'wrong',
		]);

		$response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
	}

	/*public function test_update_task()
	{
		$user = $this->createUser();

		$createResponse = $this->actingAs($user,'api')->json('POST','api/todo/tasks',[
			'title'       => $this->faker->text('20'),
			'description' => $this->faker->text('200'),
			'status'      => $this->faker->randomElement(['open','close']),
		]);
		$createResponse->assertStatus(Response::HTTP_CREATED);

		$task = $createResponse->getOriginalContent()['data']['task'];

		$newTitle       = $this->faker->text('20');
		$newDescription = $this->faker->text('200');

		$updateResponse = $this->actingAs($user,'api')->json('PATCH','api/todo/tasks/' . $task->uuid,[
			'title'       => $newTitle,
			'description' => $newDescription,
		]);
		$updateResponse->assertStatus(Response::HTTP_ACCEPTED);
		$this->assertDatabaseHas('tasks',[
			'title'       => $newTitle,
			'description' => $newDescription,
		]);
	}*/

	/**
	 * @return mixed
	 */
	private function createUser()
	{
		return $this->getUserModel()::create([
			'name'      => $this->faker->name(),
			'email'     => $this->faker->email(),
			'password'  => $this->faker->password(),
			'api_token' => Hash::make(Str::random(32)),
		]);
	}

	public function test_cannot_update_task_with_empty_title()
	{
		$token = $this->authenticate();

		// create
		$createResponse = $this->withHeaders([
			'Authorization' => 'Bearer ' . $token,
		])->json('POST','api/todo/tasks',[
			'title'       => $this->faker->text('20'),
			'description' => $this->faker->text('200'),
			'status'      => $this->faker->randomElement(['open','close']),
		]);

		$createResponse->assertStatus(Response::HTTP_CREATED);

		// update
		$task = $createResponse->getOriginalContent()['data']['task'];

		$newTitle = $this->faker->text('20');

		$updateResponse = $this->withHeaders([
			'Authorization' => 'Bearer ' . $token,
		])->json('PATCH','api/todo/tasks/' . $task->uuid,[
			'title'       => '',
			'description' => $this->faker->text('200'),
		]);

		$updateResponse->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
	}

	public function test_cannot_update_task_with_empty_description()
	{
		$token = $this->authenticate();

		// create
		$createResponse = $this->withHeaders([
			'Authorization' => 'Bearer ' . $token,
		])->json('POST','api/todo/tasks',[
			'title'       => $this->faker->text('20'),
			'description' => $this->faker->text('200'),
			'status'      => $this->faker->randomElement(['open','close']),
		]);

		$createResponse->assertStatus(Response::HTTP_CREATED);

		// update
		$task = $createResponse->getOriginalContent()['data']['task'];

		$newTitle = $this->faker->text('20');

		$updateResponse = $this->withHeaders([
			'Authorization' => 'Bearer ' . $token,
		])->json('PATCH','api/todo/tasks/' . $task->uuid,[
			'title'       => $newTitle,
			'description' => '',
		]);

		$updateResponse->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
	}

	public function test_cannot_update_task_doesnot_exist()
	{
		$user = $this->createUser();

		$updateResponse = $this->actingAs($user,'api')->json('POST','api/todo/tasks' . 'uuid',[
			'title'       => $this->faker->text('20'),
			'description' => $this->faker->text('200'),
		]);
		$updateResponse->assertStatus(Response::HTTP_NOT_FOUND);
	}

	public function test_cannot_change_status_of_task_with_wrong_status()
	{
		$token = $this->authenticate();

		// create
		$createResponse = $this->withHeaders([
			'Authorization' => 'Bearer ' . $token,
		])->json('POST','api/todo/tasks',[
			'title'       => $this->faker->text('20'),
			'description' => $this->faker->text('200'),
			'status'      => $this->faker->randomElement(['open']),
		]);

		$createResponse->assertStatus(Response::HTTP_CREATED);

		// update
		$task = $createResponse->getOriginalContent()['data']['task'];

		$updateResponse = $this->withHeaders([
			'Authorization' => 'Bearer ' . $token,
		])->json('POST','api/todo/tasks/' . $task->uuid . '/change/status',[
			'status' => 'wrong'
		]);

		$updateResponse->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
	}

	/*public function test_change_status_of_task()
	{
		$user = $this->createUser();

		$createResponse = $this->actingAs($user,'api')->json('POST','api/todo/tasks',[
			'title'       => $this->faker->text('20'),
			'description' => $this->faker->text('200'),
			'status'      => 'open',
		]);
		$createResponse->assertStatus(Response::HTTP_CREATED);

		$task = $createResponse->getOriginalContent()['data']['task'];

		$updateResponse = $this->json('POST','api/todo/tasks/' . $task->uuid . '/change/status',[
			'status' => 'close',
		]);
		$updateResponse->assertStatus(Response::HTTP_ACCEPTED);
		$this->assertDatabaseHas('tasks',[
			'status' => 'close',
		]);
	}*/

	public function test_guest_users_cannot_update_a_task()
	{
		$response = $this->withHeaders([
			'Authorization' => 'Bearer ' . 'wrong',
		])->json('POST','api/todo/tasks/' . 'wrong' . '/change/status',[
			'status' => 'wrong'
		]);

		$response->assertStatus(Response::HTTP_UNAUTHORIZED);
	}

	public function test_cannot_edit_task_of_other_users()
	{
		$user = $this->createUser();

		$createResponse = $this->actingAs($user,'api')->json('POST','api/todo/tasks',[
			'title'       => $this->faker->text('20'),
			'description' => $this->faker->text('200'),
			'status'      => $this->faker->randomElement(['open','close']),
		]);
		$createResponse->assertStatus(Response::HTTP_CREATED);

		$task = $createResponse->getOriginalContent()['data']['task'];

		$user           = $this->createUser();
		$updateResponse = $this->actingAs($user,'api')->json('PATCH','api/todo/tasks/' . $task->uuid,[
			'title'       => $this->faker->text('20'),
			'description' => $this->faker->text('200'),
		]);
		$updateResponse->assertStatus(Response::HTTP_UNAUTHORIZED);
	}

	public function test_can_notify_a_user_that_a_task_was_closed()
	{
		$user = $this->createUser();

		Notification::fake();

		$task = Task::create([
			'user_id'     => $user->id,
			'title'       => $this->faker->text('20'),
			'description' => $this->faker->text('200'),
			'status'      => 1,
		]);

		Notification::assertNothingSent();

		$user->notify(new TaskCloseEmailNotification($task));

		Notification::assertSentTo(
			$user,
			TaskCloseEmailNotification::class,
			function ($notification) use ($task) {
				return $notification->task->id === $task->id;
			}
		);
	}
}
