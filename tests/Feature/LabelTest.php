<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Tests\TestCase;

class LabelTest extends TestCase
{
	use RefreshDatabase;
	use WithFaker;

	public function getUserModel()
	{
		return config('todo.user_model');
	}

	public function test_guest_users_cannot_create_a_new_label()
	{
		$response = $this->withHeaders([
			'Authorization' => 'Bearer ' . 'wrong',
		])->json('POST','api/todo/labels',[
			'title' => $this->faker->text('20'),
		]);

		$response->assertStatus(Response::HTTP_UNAUTHORIZED);
	}

	public function test_create_label()
	{
		$token = $this->authenticate();

		$response = $this->withHeaders([
			'Authorization' => 'Bearer ' . $token,
		])->json('POST','api/todo/labels',[
			'title' => $this->faker->text('20'),
		]);

		$response->assertStatus(Response::HTTP_CREATED);
	}

	private function authenticate()
	{
		$user = $this->getUserModel()::create([
			'name'      => $this->faker->name(),
			'email'     => $this->faker->email(),
			'password'  => $this->faker->password(),
			'api_token' => Hash::make(Str::random(32)),
		]);

		return $user->api_token;
	}

	public function test_cannot_create_label_with_duplicate_title()
	{
		$token = $this->authenticate();


		$response = $this->withHeaders([
			'Authorization' => 'Bearer ' . $token,
		])->json('POST','api/todo/labels',[
			'title' => 'test',
		]);

		$response = $this->withHeaders([
			'Authorization' => 'Bearer ' . $token,
		])->json('POST','api/todo/labels',[
			'title' => 'test',
		]);

		$response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
	}

	public function test_cannot_create_label_with_empty_title()
	{
		$token = $this->authenticate();

		$response = $this->withHeaders([
			'Authorization' => 'Bearer ' . $token,
		])->json('POST','api/todo/labels',[
			'title' => '',
		]);

		$response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
	}
}
