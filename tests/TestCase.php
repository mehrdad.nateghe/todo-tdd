<?php

namespace Nateghi\Todo\tests;

use Nateghi\Todo\TodoServiceProvider;

class TestCase extends \Orchestra\Testbench\TestCase
{
	protected $user;

	public function setUp(): void
	{
		parent::setUp();
		$this->user = config('todo.user_model');
	}

	protected function getPackageProviders($app)
	{
		return [
			TodoServiceProvider::class,
		];
	}

	protected function getEnvironmentSetUp($app)
	{
		// perform environment setup
	}
}
